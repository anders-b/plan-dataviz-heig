## SVG

### Definition

Scalable (évolutif) Vector (vectoriel) Graphics
Exemples:
 
 - SVG vs PNG zoomé (évolutif)

![Exemple vectoriel vs bitmap](https://library.udel.edu/multimedia/wp-content/uploads/sites/2/2016/02/rastervvector.png)

 - Système de coordonnées

![Système de coordonnées SVG](http://dataviz-heig.surge.sh/files/svg/coordinates.svg)

### Coordonnées

Origine en haut à gauche (Attention l'axe y est l'opposé des maths à l'école)

#### `viewBox` vs. `width` + `height`

Le système de coordonnées peut être défini de deux manières:

- En spécifiant la largeur (`width`) et la hauteur (`height`) l'origin est à `(0, 0)`

```xml
<svg width="500" height="200">
  /* contenu */
</svg>
``` 

- Avec `viewBox` nous pouvons également definir l'origine (`origineX origineY largeur, hauteur`)

```xml
<svg viewBox="0 0 500 200">
  /* contenu */
</svg>
```

Original avec `viewBox="0 0 900 900"` [ouvrir](https://upload.wikimedia.org/wikipedia/commons/f/fd/Ghostscript_Tiger.svg)

Avec `viewBox="0 0 500 200"` [ouvrir](http://dataviz-heig.surge.sh/files/svg/tiger-viewBox-0-0-500-200.svg)

Avec `width="500" height="200"` [ouvrir](http://dataviz-heig.surge.sh/files/svg/tiger-width-500-height-200.svg)

Pour mieux comprendre comment fonctionne `viewBox`, voir [cet outil](http://dataviz-heig.surge.sh/files/svg/viewBox.html). Cliquez une première fois sur l'image de gauche pour définir l'origine et une seconde fois pour la largeur et la hauteur. Le `viewBox` dessiné en rouge est utilisé pour l'image de gauche.   

### Utilisation

Comme vu dans les examples ci-dessus, le `SVG` est un dialecte `XML` ([wikipedia](https://en.wikipedia.org/wiki/XML)) comme le `HTML`.

Il peut être écrit dans un fichier texte avec l'extension `.svg`. Ce fichier peut être créé et lu par un logiciel de graphisme tel que [inkscape](https://inkscape.org/). Dans ce cas il faut spécifier quel dialect `XML` est utilisé avec l'attribut `xmlns` (pour XML Name Space).

Un fichier `image-1.svg` sans cet attribut n'est par example pas compris par le navigateur.

```xml
<svg></svg>
```

[Ouvrir image-1.svg](http://dataviz-heig.surge.sh/files/svg/image-1.svg)

Un fichier `image-2.svg` avec `xmlns="http://www.w3.org/2000/svg"` ne pose pas de problèmes même si rien n'est dessiné puisqu'il est vide.

```xml
<svg xmlns="http://www.w3.org/2000/svg"></svg>
```

[Ouvrir image-2.svg](http://dataviz-heig.surge.sh/files/svg/image-2.svg)

Le `SVG` fait partie intégrante de la spécification `HTML`. Dans le contexte d'une page `HTML` il n'est pas nécessaire de définir le dialect.

Un fichier `index.html`

```xml
<html>
  <svg></svg>
</html>
```

sera compris par le navigateur.

### Les éléments de base (et leurs attributs)

#### `<rect>`

```xml
<svg width="200" height="200">
  <rect x="50" y="50" width="100" height="50" />
</svg>
```

![rect](http://dataviz-heig.surge.sh/files/svg/rect.svg)

#### `<circle>`

```xml
<svg width="200" height="200">
  <circle cx="100" cy="100" r="20" />
</svg>
```

![circle](http://dataviz-heig.surge.sh/files/svg/circle.svg)

#### `<ellipse>`

```xml
<svg width="200" height="200">
  <ellipse cx="100" cy="100" rx="30" ry="20" />
</svg>
```

![ellipse](http://dataviz-heig.surge.sh/files/svg/ellipse.svg)

#### `<line>`

```xml
<svg width="200" height="200">
  <line x1="0" y1="0" x2="100" y2="100" stroke="black" />
</svg>
```

![line](http://dataviz-heig.surge.sh/files/svg/line.svg)

#### `<text>`

```xml
<svg width="200" height="200">
  <text x="50" y="50">Salut</text>
</svg>
```

![text](http://dataviz-heig.surge.sh/files/svg/text.svg)

Pour voir tous les éléments: [MDN - SVG elements](https://developer.mozilla.org/en-US/docs/Web/SVG/Element)

### Attributs

#### Par défaut

Certains attributs sont définis par défaut. Par exemple:

```xml
<svg width="200" height="200">
  <rect x="0" y="0" width="100" height="50" />
</svg>
```

et 

```xml
<svg width="200" height="200">
  <rect width="100" height="50" />
</svg>
```

sont dessinés de la même manière, `x` et `y` sont `0` par défaut.


#### `fill`

L'attribut `fill` représente la couleur de l'élément. Il est `black` par défaut, comme dans les exemples plus haut.

```xml
<svg width="200" height="200">
  <circle cx="100" cy="100" r="20" fill="red"/>
</svg>
```

![circle red fill](http://dataviz-heig.surge.sh/files/svg/circle-fill-red.svg)

#### `stroke`

La couleur du contour

```xml
<svg width="200" height="200">
  <circle cx="100" cy="100" r="20" stroke="red"/>
</svg>
```

![circle red stroke](http://dataviz-heig.surge.sh/files/svg/circle-stroke-red.svg)


Contrairement à `fill`, `stroke` n'est pas défini par défaut. `none` permet de dire que nous ne voulons pas de couleur.

```xml
<svg width="200" height="200">
  <circle cx="100" cy="100" r="20" stroke="red" fill="none"/>
</svg>
```

![circle red stroke no fill](http://dataviz-heig.surge.sh/files/svg/circle-stroke-red-fill-none.svg)

#### `stroke-width`

La largeur du contour est `1` par défaut

```xml
<svg width="200" height="200">
  <circle cx="100" cy="100" r="20" stroke="red" fill="none" stroke-width="5"/>
</svg>
```

![circle stroke-width](http://dataviz-heig.surge.sh/files/svg/circle-stroke-width.svg)

Pour voir tous les attributs [MDN - SVG attributes](https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute)

### Ordre des éléments

Un fichier SVG est lu de haut en bas et les éléments sont dessiné au fur et à mesure.

```xml
<svg width="200" height="200">
  <circle cx="100" cy="100" r="50" fill="red"/>
  <rect x="100" y="100" width="60" height="30" fill="blue"/>
</svg>
```

![circle avant rect](http://dataviz-heig.surge.sh/files/svg/circle-above-rect.svg)

```xml
<svg width="200" height="200">
  <circle cx="100" cy="100" r="50" fill="red"/>
  <rect x="100" y="100" width="60" height="30" fill="blue"/>
</svg>
```

![rect avant circle](http://dataviz-heig.surge.sh/files/svg/rect-above-circle.svg)


### L'élément `<path>`

Pour les formes complexes. A un attribut `d` qui définit ce qui doit être dessiné.

```xml
<svg width="200" height="200">
  <path d="M 20 20 L 180 180" fill="none" stroke="red" />
</svg>
```

![path line](http://dataviz-heig.surge.sh/files/svg/path-line.svg)

Comme si l'on décrivait à quelqu'un avec un stylo ce qu'il doit faire.

`M 20 20 L 180 180`:

- `Move to 20 20`
- `Line to 180 180`

```xml
<svg width="200" height="200">
  <path d="M 20 20 L 180 180 L 20 180" fill="none" stroke="red" />
</svg>
```

On peut ajouter autant de points que l'on veut.

![path line](http://dataviz-heig.surge.sh/files/svg/path-multiline.svg)

```xml
<svg width="200" height="200">
  <path d="M 20 20 L 180 180 L 20 180 Z" fill="none" stroke="red" />
</svg>
```

`Z` à la fin pour revenir au point de départ

![path triangle](http://dataviz-heig.surge.sh/files/svg/path-triangle.svg)

```xml
<svg width="200" height="200">
  <path d="M 20 100 L 180 100" fill="none" stroke="red" />
  <path d="M 20 100 C 50 150 150 150 180 100" fill="none" stroke="green" />
</svg>
```

La commande `C` permet de dessiner une courbe. Les deux lignes ont les même points de départ (20, 100) et d'arrivée (180, 100). `50 150` et `150 150` entre `C` et `180 100` sont les points de contrôle.

![path curve](http://dataviz-heig.surge.sh/files/svg/path-curve.svg)


Pour plus de détails [MDN paths](https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths)

### L'élément `<g>`

`<g>` permet de grouper des éléments

```xml
<svg width="200" height="200">
  <g fill="yellow" stroke="purple">
    <circle cx="100" cy="100" r="30" />
    <rect x="80" y="80" width="60" height="30" />
  </g>
</svg>
```

![g](http://dataviz-heig.surge.sh/files/svg/group.svg)

### L'attribut `transform`

#### `translate(x, y)`

```xml
<svg width="200" height="200">
  <g fill="yellow" stroke="purple" transform="translate(-50, -50)">
    <circle cx="100" cy="100" r="30" />
    <rect x="80" y="80" width="60" height="30" />
  </g>
</svg>
```

![translate](http://dataviz-heig.surge.sh/files/svg/transform-translate.svg)

#### `scale(x, y?)`

```xml
<svg width="200" height="200">
  <g fill="yellow" stroke="purple" transform="scale(1.3)">
    <circle cx="100" cy="100" r="30" />
    <rect x="80" y="80" width="60" height="30" />
  </g>
</svg>
```

![scale](http://dataviz-heig.surge.sh/files/svg/transform-scale.svg)

```xml
<svg width="200" height="200">
  <g fill="yellow" stroke="purple" transform="scale(1.3, 0.5)">
    <circle cx="100" cy="100" r="30" />
    <rect x="80" y="80" width="60" height="30" />
  </g>
</svg>
```

![scale xy](http://dataviz-heig.surge.sh/files/svg/transform-scale-xy.svg)

#### rotate(deg, x?, y?)

```xml
<svg width="200" height="200">
  <g fill="yellow" stroke="purple" transform="rotate(45, 100, 100)">
    <circle cx="100" cy="100" r="30" />
    <rect x="80" y="80" width="60" height="30" />
  </g>
</svg>
```

![rotate](http://dataviz-heig.surge.sh/files/svg/transform-rotate.svg)

Pour plus de details, [MDN transform](https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/transform)

### Intégrer SVG dans une page web

```xml
<img src="image.svg" alt="Image SVG">
```

```xml
<object data="image.svg" type="image/svg+xml">
  <img src="fallback.jpg" />
</object>
```

```xml
<svg></svg>
```

### SVG et CSS

```html
<html>
  <head>
    <style>
      #cercle {
        fill: red;
        stroke: black;
      }
      #cercle:hover {
        fill: blue;
        stroke: green;
      }
    </style>
  </head>
  <body>
      <svg viewBox="0 0 100 50">
        <circle id="cercle" cx="50" cy="25" r="20" />
      </svg>
  </body>
</html>
```

[Ouvrir](http://dataviz-heig.surge.sh/files/svg/css.html)

### SVG et javascript

#### Event

```html
<html>
  <body>
      <svg viewBox="0 0 100 50">
        <circle id="cercle" cx="50" cy="25" r="20" />
      </svg>
      <script>
        const cercle = document.getElementById('cercle')
        cercle.addEventListener(
          'click',
          () => cercle.setAttribute('r', Number(cercle.getAttribute('r') - 1))
        )
      </script>
  </body>
</html>
```

[Ouvrir](http://dataviz-heig.surge.sh/files/svg/js-event.html)

#### Animation

```html
<html>
  <body>
      <svg viewBox="0 0 100 50">
        <circle
          id="cercle"
          cx="50"
          cy="25"
          r="20"
          fill="none"
          stroke="black"
        />
        <line
          id="s"
          x1="50"
          y1="25"
          x2="50"
          y2="7"
          stroke="red"
          stroke-width="0.3"
        />
        <line
          id="m"
          x1="50"
          y1="25"
          x2="50"
          y2="10"
          stroke="black"
          stroke-width="0.6"
          stroke-linecap="round"
        />
        <line
          id="h"
          x1="50"
          y1="25"
          x2="50"
          y2="15"
          stroke="black"
          stroke-width="0.9"
          stroke-linecap="round"
        />
      </svg>
      <script>
        const center = '50,25'
        const s = document.getElementById('s')
        const m = document.getElementById('m')
        const h = document.getElementById('h')
        const getHourDeg = hour => (hour > 11 ? hour - 12 : hour) * 30
        const setTime = () => {
          const now = new Date()
          s.setAttribute('transform', `rotate(${now.getSeconds() * 6},${center})`)
          m.setAttribute('transform', `rotate(${now.getMinutes() * 6},${center})`)
          h.setAttribute('transform', `rotate(${getHourDeg(now.getHours())},${center})`)
          window.requestAnimationFrame(setTime)
        }
        window.requestAnimationFrame(setTime)
      </script>
  </body>
</html>
```

[Ouvrir](http://dataviz-heig.surge.sh/files/svg/js-animation.html)

### Liens

#### Vidéos

- [Building Better Interfaces With SVG](https://www.youtube.com/watch?v=lMFfTRiipOQ)
- [SVG can do that?!](https://www.youtube.com/watch?v=ADXX4fmWHbo)

#### Tutoriels

- [MDN SVG tutorial](https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial)