const fs = require('fs')

const folders = [
  'svg',
  'd3',
  'billboard',
  'd3-rosling',
]

const getFolderAssets = folder => {
  const files = fs.readdirSync(`${folder}/assets`)
  return files && files.length !== 0
    ? files.map(file => ({ input: `${folder}/assets/${file}`, output: `assets/files/${folder}/${file}` }))
    : []
}

const assetsList = folders.map(getFolderAssets).reduce((result, list) => ([...result, ...list]), [])

const copyFile = ({ input, output }) => new Promise((resolve, reject) => {
  console.log(`COPYING ${input} TO ${output}`)
  fs.readFile(input, (err, file) => {
    if (err) { return reject(err) }
    fs.writeFile(output, file, err => {
      return err ? reject(err) : resolve()
    })
  })
})
console.log(assetsList)
Promise.all(assetsList.map(copyFile))
  .then(() => console.log('done'))
  .catch(err => console.log(err))
