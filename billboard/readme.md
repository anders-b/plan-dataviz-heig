## Billboard

[Site](https://naver.github.io/billboard.js/)

### Utilisation

```html
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://unpkg.com/billboard.js@1.6.2/dist/billboard.min.css">
  </head>
  <body>
    <div id="graph"></div>
    <script src="https://unpkg.com/billboard.js@1.6.2/dist/billboard.pkgd.min.js"></script>
    <script>
    </script>
  </body>
</html>
```

### Graphique en bâtons

Nos données:

```javascript
const DATA = [
  { nom: 'Lausanne', population: 138905 },
  { nom: 'Yverdon', population: 30143 },
  { nom: 'Montreux', population: 26574 },
  { nom: 'Renens', population: 21036 },
  { nom: 'Nyon', population: 20533 },
  { nom: 'Vevey', population: 19827 },
]
```

Charger les données et attacher le graphique à `#graph`:

```javascript
bb.generate({
  data: {
    columns: [
      ['Population', ...DATA.map(({ population }) => population)],
    ],
  },
  bindto: '#graph'
})
```

Les données sont sous forme de colonnes. Elles commencent avec le nom suivi des données. Le type de graphique par défaut est linéaire.

![Charger les données](http://dataviz-heig.surge.sh/files/billboard/bar-1.png)

En faire un graphique en bâtons:

```javascript
bb.generate({
  data: {
    columns: [
      ['Population', ...DATA.map(({ population }) => population)],
    ],
    type: 'bar', // <-- nouveau
  },
  bindto: '#graph'
})
```

![Graphique en bâtons](http://dataviz-heig.surge.sh/files/billboard/bar-2.png)

Ajouter le nom des villes sur l'axe `x`:

```javascript
bb.generate({
  data: {
    columns: [
      ['Population', ...DATA.map(({ population }) => population)],
    ],
    type: 'bar',
  },
  /* Nouveau - début */
  axis: {
    x: {
      type: 'category',
      categories: DATA.map(({ nom }) => nom),
    }
  },
  /* Nouveau - fin */
  bindto: '#graph'
})
```

![Catégories](http://dataviz-heig.surge.sh/files/billboard/bar-3.png)

Changer la couleur des bâtons:

```javascript
bb.generate({
  data: {
    columns: [
      ['Population', ...DATA.map(({ population }) => population)],
    ],
    type: 'bar',
  },
  axis: {
    x: {
      type: 'category',
      categories: DATA.map(({ nom }) => nom)
    }
  },
  /* Nouveau - début */
  color: {
    pattern: ['red'],
  },
  /* Nouveau - fin */
  bindto: '#graph'
})
```

![Couleur des bâtons](http://dataviz-heig.surge.sh/files/billboard/bar-4.png)

### Ajouter une série de données

```javascript
const DATA = [
  { nom: 'Lausanne', population: 138905, superficie: 41.38 },
  { nom: 'Yverdon', population: 30143, superficie: 11.28 },
  { nom: 'Montreux', population: 26574, superficie: 33.37 },
  { nom: 'Renens', population: 21036, superficie: 2.96 },
  { nom: 'Nyon', population: 20533, superficie: 6.79 },
  { nom: 'Vevey', population: 19827, superficie: 2.38 },
]
```

```javascript
bb.generate({
  data: {
    columns: [
      ['Population', ...DATA.map(({ population }) => population)],
      // Ajouter une colonne de données
      ['Superficie', ...DATA.map(({ superficie }) => superficie)],
    ],
    // Ajouter un axe y2
    axes: {
      Superficie: 'y2'
    },
    type: 'bar',
  },
  axis: {
    x: {
      type: 'category',
      categories: DATA.map(({ nom }) => nom)
    },
    // Nommer l'axe y
    y: {
      label: 'Population',
    },
    // Montrer et nommer l'axe y2
    y2: {
      show: true,
      label: 'Superficie',
    }
  },
  bindto: '#graph'
})
```

Si on avait une autre série en rapport avec la population ("Population en 1900" par example), il aurait suffit d'ajouter une colonne.

![Graphique en bâtons avec 2 série](http://dataviz-heig.surge.sh/files/billboard/bar-5.png)

[Résultat](http://dataviz-heig.surge.sh/files/billboard/bar.html)

### Les axes

![Prix du bitcoin en dollars](http://dataviz-heig.surge.sh/files/billboard/line-2.png)

Réutilisons le jeu de données avec le prix des bitcoins

```javascript
// Chercher les données dans un autre fichier
const getData = () =>
  fetch('bitcoin.json')
    .then(res => res.json())

// Créer une série de dates et une série de prix
const formatData = ({ bpi }) => {
  const dates = Object.keys(bpi)
  const values = dates.map(key => bpi[key])
  return { dates, values }
}

const drawGraph = ({ dates, values }) => {
  bb.generate({
    data: {
      x: 'x',
      columns: [
        ['x', ...dates],
        ['Bitcoin in USD', ...values],
      ]
    },
    axis: {
      x: {
        type: 'timeseries',
        tick: {
          // le nombre d'intervales que nous souhaitons
          count: 5,
          // le format des données
          format: '%Y-%m-%d'
        }
      },
      y: {
        tick: {
          // Remplacer les milliers par "k"
          format: d => `${d / 1000}k`,
        },
      },
    },
    point: {
      // ne pas montrer un point par valeur, voir plus bas
      show: false,
    },
    bindto: '#graph'
  })
}

getData()
  .then(formatData)
  .then(drawGraph)
```

Si nous laissons les points, le graphique ressemble à ça:

![Ligne avec points](http://dataviz-heig.surge.sh/files/billboard/line-1.png)

[Résultat](http://dataviz-heig.surge.sh/files/billboard/line.html)

### Nuage de points

![Nuage de points](http://dataviz-heig.surge.sh/files/billboard/scatter-1.png)

```javascript
const DATA = [
  { temperature: 14.2, iceCreamSales: 215 },
  { temperature: 16.4, iceCreamSales: 325 },
  { temperature: 11.9, iceCreamSales: 185 },
  { temperature: 15.2, iceCreamSales: 332 },
  { temperature: 18.5, iceCreamSales: 406 },
  { temperature: 22.1, iceCreamSales: 522 },
  { temperature: 19.4, iceCreamSales: 412 },
  { temperature: 25.1, iceCreamSales: 614 },
  { temperature: 23.4, iceCreamSales: 544 },
  { temperature: 18.1, iceCreamSales: 421 },
  { temperature: 22.6, iceCreamSales: 445 },
  { temperature: 17.2, iceCreamSales: 408 },
]

bb.generate({
  data: {
    xs: {
      // l'axe x de iceCreamSales est "temperature"
      iceCreamSales: 'temperature',
    },
    columns: [
      ['temperature', ...DATA.map(({ temperature}) => temperature)],
      ['iceCreamSales', ...DATA.map(({ iceCreamSales }) => iceCreamSales)],
    ],
    type: "scatter"
  },
  point: {
    // le "point" peut avoir la forme que l'on veut
    pattern: [
      '<circle r="10" transform="translate(10, 10)"></circle>'
    ]
  },
  // nommer les axes
  axis: {
    x: {
      label: 'Température',
    },
    y: {
      label: 'Ventes de glaces en CHF',
    },
  },
  // pas de légende
  legend: {
    show: false
  },
  // pas d'information en passant au dessus des points
  tooltip: {
    show: false
  },
  bindto: '#graph'
})
```

[Résultat](http://dataviz-heig.surge.sh/files/billboard/scatter.html)

### Diagramme circulaire

![Diagramme circulaire](http://dataviz-heig.surge.sh/files/billboard/pie-1.png)

```javascript
const DATA = [
  { nom: 'Baptiste', transport: 'Vélo' },
  { nom: 'Barthélemy', transport: 'Vélo' },
  { nom: 'Bastien', transport: 'Bus' },
  { nom: 'Baudouin', transport: 'Voiture' },
  { nom: 'Beau', transport: 'Vélo' },
  { nom: 'Benoît', transport: 'Bus' },
  { nom: 'Bernard', transport: 'Vélo' },
  { nom: 'Bertrand', transport: 'Vélo' },
  { nom: 'Blaise', transport: 'Voiture' },
  { nom: 'Blanchard', transport: 'Bus' },
  { nom: 'Bruno', transport: 'Vélo' },
  { nom: 'Barbara', transport: 'Voiture' },
  { nom: 'Béatrice', transport: 'Vélo' },
  { nom: 'Bénédicte', transport: 'Bus' },
  { nom: 'Bernadette', transport: 'Bus' },
  { nom: 'Bijou', transport: 'Voiture' },
  { nom: 'Blanche', transport: 'Vélo' },
  { nom: 'Blanchefleur', transport: 'Vélo' },
  { nom: 'Brigitte', transport: 'Vélo' },
]

const getDataByTransport = t =>
  DATA
    .filter(({ transport }) => transport === t)
    .map(() => 1)

const columns = ['Vélo', 'Bus', 'Voiture']
  .map(t => ([t, ...getDataByTransport(t)]))

/*
[
  ['Vélo',1,1,1,1,1,1,1,1,1,1],
  ['Bus',1,1,1,1,1],
  ['Voiture',1,1,1,1]
]
*/

bb.generate({
  data: {
    columns,
    type: 'pie',
  },
  bindto: '#graph'
})
```

[Résultat](http://dataviz-heig.surge.sh/files/billboard/pie.html)

### Donut

![Donut](http://dataviz-heig.surge.sh/files/billboard/donut.png)

Pour avoir un diagramme circulaire en forme de donut, il suffit de changer le type de graphique.

```javascript
bb.generate({
  data: {
    columns,
    type: 'donut', // <-- nouveau
  },
  bindto: '#graph'
})
```

[Résultat](http://dataviz-heig.surge.sh/files/billboard/donut.html)

### Radar

![Radar](http://dataviz-heig.surge.sh/files/billboard/radar-1.png)

```javascript
const DATA = [
  { hero: 'Captain America', skill: 'Intelligence', value: 3 },
  { hero: 'Captain America', skill: 'Strength', value: 3 },
  { hero: 'Captain America', skill: 'Speed', value: 2 },
  { hero: 'Captain America', skill: 'Durability', value: 3 },
  { hero: 'Captain America', skill: 'Energy', value: 1 },
  { hero: 'Captain America', skill: 'Fighting Skills', value: 6 },
  { hero: 'Iron Man', skill: 'Intelligence', value: 6 },
  { hero: 'Iron Man', skill: 'Strength', value: 6 },
  { hero: 'Iron Man', skill: 'Speed', value: 5 },
  { hero: 'Iron Man', skill: 'Durability', value: 6 },
  { hero: 'Iron Man', skill: 'Energy', value: 6 },
  { hero: 'Iron Man', skill: 'Fighting Skills', value: 4 },
  { hero: 'Hulk', skill: 'Intelligence', value: 6 },
  { hero: 'Hulk', skill: 'Strength', value: 7 },
  { hero: 'Hulk', skill: 'Speed', value: 3 },
  { hero: 'Hulk', skill: 'Durability', value: 7 },
  { hero: 'Hulk', skill: 'Energy', value: 1 },
  { hero: 'Hulk', skill: 'Fighting Skills', value: 4 },
  { hero: 'Thor', skill: 'Intelligence', value: 2 },
  { hero: 'Thor', skill: 'Strength', value: 7 },
  { hero: 'Thor', skill: 'Speed', value: 7 },
  { hero: 'Thor', skill: 'Durability', value: 6 },
  { hero: 'Thor', skill: 'Energy', value: 6 },
  { hero: 'Thor', skill: 'Fighting Skills', value: 4 },
]
// source: http://bl.ocks.org/chrisrzhou/raw/2421ac6541b68c1680f8/e9fe262498c65161f13838d9fd08f87f895a7644/data_the_avengers.csv

const uniq = arr => arr.reduce((result, item) => result.includes(item) ? result : [...result, item], [])
/*
  retourne les éléments uniques d'une liste
  ex: uniq(['A', 'A', 'B']) -> ['A', 'B']
*/

const getHeroSkillValue = (hero, skill) => DATA.find(item => item.hero === hero && item.skill === skill).value
// prends un "hero" et un "skill" et retourne la valeur "value"

const skills = uniq(DATA.map(({ skill }) => skill))
// tous les "skills"

const heros = uniq(DATA.map(({ hero }) => hero))
// tous les "heros"

const columns = [
  ['x', ...skills],
  ...heros.map(hero => ([hero, ...skills.map(skill => getHeroSkillValue(hero, skill))]))
]

/*
[
  ["x","Intelligence","Strength","Speed","Durability","Energy","Fighting Skills"],
  ["Captain America",3,3,2,3,1,6],
  ["Iron Man",6,6,5,6,6,4],
  ["Hulk",6,7,3,7,1,4],
  ["Thor",2,7,7,6,6,4]
]
*/

bb.generate({
  data: {
    x: 'x',
    columns,
    type: 'radar',
  },
  bindto: '#graph',
})
```

[Résultat](http://dataviz-heig.surge.sh/files/billboard/radar.html)