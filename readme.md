## Plan de cours

### Introduction à la visualisation de données

### [SVG](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/svg)

* [définition](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/svg#definition)
* [coordonnées](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/svg#coordonn%C3%A9es)
* [utilisation](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/svg#utilisation)
* [éléments de base](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/svg#les-%C3%A9l%C3%A9ments-de-base-et-leurs-attributs)
* [attributs](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/svg#attributs)
* [ordre de lecture](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/svg#ordre-des-%C3%A9l%C3%A9ments)
* [l'élément path](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/svg#l%C3%A9l%C3%A9ment-path)
* [l'élément g](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/svg#l%C3%A9l%C3%A9ment-g)
* [l'attribut transform](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/svg#lattribut-transform)
* [SVGet HTML](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/svg#int%C3%A9grer-svg-dans-une-page-web)
* [SVG et CSS](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/svg#svg-et-css)
* [SVG et JS](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/svg#svg-et-javascript)

### [Introdiction à D3](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/d3)

* [select](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/d3#selection-dom)
* [data](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/d3#joindre-des-donn%C3%A9es-%C3%A0-un-%C3%A9l%C3%A9ment)
* [graphique en bâtons](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/d3#svg-un-graphique-en-b%C3%A2tons)
* [scale](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/d3#les-%C3%A9chelles-avec-d3scale)
* [axis](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/d3#les-axes-avec-d3axis)
* [events](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/d3#%C3%A9v%C3%A8nements)
* [animation](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/d3#animation)
* [graphique en courbe](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/d3#graphique-en-courbe-avec-line)
* [repésenter un réseau](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/d3#représenter-un-réseau)

### [Billboard](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/billboard)

* [utilisation](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/billboard#utilisation)
* [graphique en bâtons](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/billboard#graphique-en-b%C3%A2tons)
* [ajouter une série de données](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/billboard#ajouter-une-s%C3%A9rie-de-donn%C3%A9es)
* [axes](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/billboard#les-axes)
* [nuage de points](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/billboard#nuage-de-points)
* [diagrame circulaire](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/billboard#diagramme-circulaire)
* [donut](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/billboard#donut)
* [radar](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/billboard#radar)

### Utiliser D3

* [Graphique Rosling](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/d3-rosling)

### Abstractions de D3

### Scraping
