## D3

### Selection DOM

#### Pour avoir accès à un ou plusieurs éléments

* `.select()` pour le premier élément
* `.selectAll()` pour tous les éléments

#### Méthodes pour modifier la selection

* `.attr()` pour lire / ajouter / modifier un attribut
* `.style()` pour lire / ajouter / modifier le style attribut
* `.text()` pour ajouter / modifier le texte

Imaginons un élément: 

```html
<p></p>
```

```javascript
const p =  select('p')
p.attr('id', 'mon-paragraphe')
// <p id="mon-paragraphe"></p>
```

```javascript
const p =  select('p')
p.style('color', 'red')
// <p style="color:red;"></p>
```

```javascript
const p =  select('p')
p.text('Salut')
// <p>Salut</p>
```

#### Ajouter un élément "enfant"

* `.append()`

Toujours avec: 

```html
<p></p>
```

```javascript
const p =  d3.select('p')
const span = p.append('span').text('Salut')
// <p><span>Salut</span></p>
```

### Joindre des données à un élément

```javascript
const data = [4, 6, 2, 8, 1]
```

`.selectAll()` permet de selectionner des éléments qui n'existe pas encore mais qui vont être créer en fonction de nos données `data`

```html
<div id="text"></div>
```

```javascript
const text = d3.select('text') // l'élément existant

const paragraphs = text.selectAll('p') // <p> qui n'existent pas
  .data(data) // ajouter un <p> par entrée

paragraphs.enter() // quand une donnée entre
  .append('p') // ajouter un élément <p>
  .text(d => `Paragraphe ${d}`) // utiliser la donnée pour le contenu
```

Résultat

```html
<div id="text">
  <p>Paragraphe 4</p>
  <p>Paragraphe 6</p>
  <p>Paragraphe 2</p>
  <p>Paragraphe 8</p>
  <p>Paragraphe 1</p>
</div>
```

Une fois qu'une liste d'éléments a été jointe à une liste de données, la donnée est accessible avec la fonction `(d, i) => ` où `d` représente la donnée (`4`, par exemple) et `i` l'indexe.

Pour changer la couleur en fonction de la donnée:

```javascript
paragraphs.enter()
  .append('p')
  .style('color', d => d > 3 ? 'red' : 'green') // si "d" est plus grand que 3, "red" sinon "green"
  .text(d => `Paragraphe ${d}`)
```

```html
<div id="text">
  <p style="color:red;">Paragraphe 4</p>
  <p style="color:red;">Paragraphe 6</p>
  <p style="color:green;">Paragraphe 2</p>
  <p style="color:red;">Paragraphe 8</p>
  <p style="color:green;">Paragraphe 1</p>
</div>
```

Pour changer le contenu en fonction de l'indexe

```javascript
paragraphs.enter()
  .append('p')
  .text((d, i) => `Index: ${i}`)
```

```html
<div id="text">
<p>Index: 0</p>
<p>Index: 1</p>
<p>Index: 2</p>
<p>Index: 3</p>
<p>Index: 4</p>
</div>
```

La liste de données n'a pas besoin d'être une lise de chiffres.

Par exemple:

```javascript
const villes = [
  { nom: 'Lausanne', population: 138905 },
  { nom: 'Yverdon-les-Bains', population: 30143 },
  { nom: 'Montreux', population: 26574 },
  { nom: 'Renens', population: 21036 },
  { nom: 'Nyon', population: 20533 },
  { nom: 'Vevey', population: 19827 },
]
```

Dans ce cas `d` représente toujours la données (par exemple `{ nom: 'Lausanne', population: 138905 }`) mais cette fois c'est un objet

```javascript
const lausanne = { nom: 'Lausanne', population: 138905 }
const nom = d => d.nom
const pop = d => d.population

console.log(nom(lausanne)) // 'Lausanne'
console.log(pop(lausanne)) // 138905
```

Voir un [exemple](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/d3/assets/villes.html) d'utilsation et le [résultat](http://dataviz-heig.surge.sh/files/d3/villes.html)

### SVG - un graphique en bâtons

![SVG bâtons](http://dataviz-heig.surge.sh/files/d3/batons.svg)

[Code](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/d3/assets/batons.html)

[Résultat](http://dataviz-heig.surge.sh/files/d3/batons.html)

### Les échelles avec `d3.scale`

Dans l'exemple nous avons tricher un peu puisque les chiffres dans `data` sont entre `0` et `HEIGHT`, la hauteur du graphique.

Si nous remplaçons

```javascript
const DATA = Array.from(Array(10)).map(() => Math.floor(Math.random() * HEIGHT))
```

par

```javascript
const DATA = Array.from(Array(10)).map(() => Math.random())
```

Les bâtons deviennent invisibles. Il nous faut utiliser une autre échelle pour la hauteur des bâtons.

[Documentation d3-scale](https://github.com/d3/d3-scale)

Un exemple simple

```javascript
const scale = d3.scaleLinear() // échelle linéaire
  .domain([0, 10]) // minimum et maxium des données entrantes
  .range([0, 100]) // minimum et maximum des données sortantes

console.log(scale(1)) // 10
console.log(scale(3.5)) // 35
```

Dans notre cas:

```javascript
const scaleY = d3.scaleLinear()
  .domain([0, d3.max(DATA)])
  .range([HEIGHT, 0])
```

L'échelle prends `0` comme minimum et la donnée maxium de `DATA` en entrée. `HEIGHT` comme miniumum et `0` en maximum en sortie. 

Et utilisons `scaleY()` pour calculer `y` et `height`:

```javascript
  // ...
  .attr('y', d => scaleY(d))
  .attr('height', d => HEIGHT - scaleY(d))  
```

[Code modifié](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/d3/assets/echelles.html) 

[Résultat](http://dataviz-heig.surge.sh/files/d3/echelles.html)


### Les axes avec `d3.axis`

Jusqu'ici le graphique ne dit pas grand chose sur ce qui est représenté. Les données ne représentent rien de toutes manière. Revenons à nos villes. Il nous faut ajouter un axe pour le nombre d'habitants et le nom de la ville en dessous de chaque bâton.

Les graphiques précèdants prenaient la totalité du SVG, cette fois nous devons laisser un peu de place pour les axes.

```javascript
// les constantes pour la taille du graphique
const WIDTH_MARGIN = 50
const HEIGHT_MARGIN = 30
const GRAPH_WIDTH = WIDTH - WIDTH_MARGIN
const GRAPH_HEIGHT = HEIGHT - HEIGHT_MARGIN

// mise à jour des échelles
const scaleX = d3.scaleLinear()
  .domain([0, DATA.length])
  .range([0, GRAPH_WIDTH])

const scaleY = d3.scaleLinear()
  .domain([0, d3.max(DATA, d => d.population)])
  .range([GRAPH_HEIGHT, 0])

// un groupe pour contenir les bâtons
const barsGroup = svg.append('g')
  .attr('transform', `translate(${WIDTH_MARGIN})`)

// attacher les bâtons au groupe
const bars = barsGroup.selectAll('rect')
  .data(DATA)
  .enter()
  .append('rect')
  .attr('x', (d, i) =>  scaleX(i))
  .attr('width', (GRAPH_WIDTH / DATA.length) - MARGIN)
  .attr('y', d => scaleY(d.population))
  .attr('height', d => GRAPH_HEIGHT - scaleY(d.population)) 
```

Maintenant ajoutons un axe Y

```javascript
// créer un axe en fonction de l'échelle
const axisY = d3.axisLeft().scale(scaleY)

// créer un nouveau groupe et y attacher l'axe
svg.append('g')
  .attr('transform', `translate(${WIDTH_MARGIN - 3})`)
  .call(axisY)
```

Un axe contient plusieurs éléments:

* la ligne le long du graphique
* une ligne pour chaque unité
* un texte pour chaque unité

`d3.axis` créer tous ces éléments "par magie", il calcule aussi le nombre de "ticks" en fonction de l'unité et de l'espace disponible.

[Documentation d3-axis](https://github.com/d3/d3-axis)

Pour les noms de villes

```javascript
// une constante pour la largeur des bâtons
const BAR_WIDTH = (GRAPH_WIDTH / DATA.length) - MARGIN

// une constante pour la taille du texte
const LABEL_SIZE = 10

// un groupe pour l'axe X
const axisX = svg.append('g')
  .attr('transform', `translate(${WIDTH_MARGIN})`)

// ajouter les noms au groupe
const labels = axisX.selectAll('text')
  .data(DATA)
  .enter()
  .append('text')
  .attr('text-anchor', 'middle')
  .attr('font-size', LABEL_SIZE)
  .attr('x', (d, i) => scaleX(i) + BAR_WIDTH / 2)
  .attr('y', HEIGHT - HEIGHT_MARGIN / 2)
  .text(d => d.nom)
```

![Bâtons avec axes](http://dataviz-heig.surge.sh/files/d3/axes.svg)

[Code](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/d3/assets/axes.html)

[Résultat](http://dataviz-heig.surge.sh/files/d3/axes.html)

### Évènements

`d3` permet d'attacher un évènement aux éléments créés

```javascript
bars.on('click', ({ nom, population }) =>
  alert(`${nom} a ${population} habitants`))
```

En cliquant sur le premier bâtons nous recevons une alerte `Lausanne a 138905 habitants`.

```javascript
bars.on('mouseover', function() {
  d3.select(this)
    .attr('fill', 'red')
})
```

Quand la souris passe au dessus d'un bâton, il change de couleur.

Problème: il reste rouge quand la souris n'est plus dessus

```javascript
bars.on('mouseout', function() {
  d3.select(this)
    .attr('fill', 'black')
})
```

Notez que nous utilisons une fonction `function()` à l'ancienne ici. Les fonctions `() =>`, n'ont pas de contexte `this`.

[Documentation d3 events .on()](https://github.com/d3/d3-selection/blob/master/README.md#selection_on)

[Code](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/d3/assets/events.html)

[Résultat](http://dataviz-heig.surge.sh/files/d3/events.html)

### Animation

Imaginons que la taille des bâtons est de `0` lors du chargement de la page.

```javascript
const bars = svg.selectAll('rect')
  .data(DATA)
  .enter()
  .append('rect')
  .attr('x', (d, i) =>  scaleX(i))
  .attr('width', BAR_WIDTH)
  .attr('y', HEIGHT) // y est tout en bas
  .attr('height', 0) // la hauteur est de 0
```

Pour que les bâtons "grandissent" en une seconde:

```javascript
bars.transition()
  .duration(1000)
  // les mêmes valeurs que pour les exemple précèdants
  .attr('y', d => scaleY(d.population))
  .attr('height', d => HEIGHT - scaleY(d.population))
```

[Code](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/d3/assets/anim-1.html)

[Résultat](http://dataviz-heig.surge.sh/files/d3/anim-1.html)

Ici la durée est en millisecondes

Pour que les bâtons grandissent les un après les autres:

```javascript
bars.transition()
  .delay((d, i) => i * TIME)
  .duration(TIME)
  .attr('y', d => scaleY(d.population))
  .attr('height', d => HEIGHT - scaleY(d.population))
```

[Code](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/d3/assets/anim-2.html)

[Résultat](http://dataviz-heig.surge.sh/files/d3/anim-2.html)

Un délais `.delay()` (toujours en millisecondes) est ajouté en fonction de l'indexe de la données

[Documentation d3-transition](https://github.com/d3/d3-transition)

### Graphique en courbe avec `.line()`

![Graphique en courbe](http://dataviz-heig.surge.sh/files/d3/courbe.svg)

Nous allons utiliser ce [jeu de donnée](https://gitlab.com/anders-b/plan-dataviz-heig/raw/master/d3/assets/bitcoin.json) avec les prix de cloture du bitcoin en USD.

```javascript
fetch('bitcoin.json')
  .then(response => resonse.json())
  .then(console.log)
```

Premier problème: ce n'est pas une liste mais un objet

```json
{
  "bpi": {
    "2018-01-01": 13412.44,
    "2018-01-02": 14740.7563,
    "2018-01-03": 15134.6513,
    //...
  }
}
```

```javascript
// nous voulons créer une liste à partir de la clé "bpi"
const formatData = ({ bpi }) =>
  Object.keys(bpi)
    // ["2018-01-01", "2018-01-02", ...]
    .map(key => ({ date: key, close: bpi[key] }))
    // [{ date: "2018-01-01", close: 13412.44 }, ...]
```

Pour les dates, nous devons dire à `d3` sous quel format elles se présentent

[Documentation d3-time](https://github.com/d3/d3-time)

```javascript
const formatDate = d3.timeParse('%Y-%m-%d')
const formatData = ({ bpi }) =>
  Object.keys(bpi)
    .map(key => ({ date: formatDate(key), close: bpi[key] }))

fetch('bitcoin.json')
  .then(response => response.json())
  .then(formatData)
  .then(drawLine) // créer le graphique une fois que nous avons les données
```

Définir `drawLine()`

```javascript
const WIDTH = 500
const HEIGHT = 200

const body = d3.select('body')

const svg = body.append('svg')
  .attr('width', WIDTH)
  .attr('height', HEIGHT)

const drawLine = data => {

  // échelle de temps pour l'axe X
  const scaleX = d3.scaleTime()
    .range([0, WIDTH])
    .domain(d3.extent(data, d => d.date))
  
  // échelle linéaire pour le prix de cloture
  const scaleY = d3.scaleLinear()
    .range([HEIGHT, 0])
    .domain(d3.extent(data, d => d.close))

  // la fonction d3.line() pour créer l'attribut "d"
  const linePathCreator = d3.line() 
    // quelle échelle, quelle donnée pour l'axe X
    .x(d => scaleX(d.date))
    // quelle échelle, quelle donnée pour l'axe Y
    .y(d => scaleY(d.close))

  // ajouter une courbe au SVG
  const line = svg.append('path')
    // utiliser linePathCreator pour créer l'attribut "d"
    .attr('d', linePathCreator(data))
    .attr('fill', 'none')
    .attr('stroke', 'red')
}
```

[Code](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/d3/assets/courbe.html)

[Résultat](http://dataviz-heig.surge.sh/files/d3/courbe.html)

### Représenter un réseau

Les données

```javascript
const DATA = {
  nodes: [
    {  id: 'Baptiste', group: 1 },
    {  id: 'Barthélemy', group: 1 },
    {  id: 'Bastien', group: 2 },
    {  id: 'Baudouin', group: 3 },
    {  id: 'Beau', group: 1 },
    {  id: 'Benoît', group: 2 },
    {  id: 'Bernard', group: 1 },
    {  id: 'Bertrand', group: 1 },
    {  id: 'Blaise', group: 3 },
    {  id: 'Blanchard', group: 2 },
    {  id: 'Bruno', group: 1 },
    {  id: 'Barbara', group: 3 },
    {  id: 'Béatrice', group: 1 },
    {  id: 'Bénédicte', group: 2 },
    {  id: 'Bernadette', group: 2 },
    {  id: 'Bijou', group: 3 },
    {  id: 'Blanche', group: 1 },
    {  id: 'Blanchefleur', group: 1 },
    {  id: 'Brigitte', group: 1 },
  ],
  edges: [
    { source: 'Brigitte', target: 'Blanchefleur' },
    { source: 'Brigitte', target: 'Blanche' },
    { source: 'Brigitte', target: 'Bijou' },
    { source: 'Brigitte', target: 'Bernadette' },
    { source: 'Brigitte', target: 'Bénédicte' },
    { source: 'Brigitte', target: 'Béatrice' },
    { source: 'Brigitte', target: 'Barbara' },
    { source: 'Brigitte', target: 'Bruno' },
    { source: 'Brigitte', target: 'Blanchard' },
    { source: 'Bruno', target: 'Blanchard' },
    { source: 'Bruno', target: 'Blaise' },
    { source: 'Bruno', target: 'Bertrand' },
    { source: 'Bruno', target: 'Bernard' },
    { source: 'Bernard', target: 'Benoît' },
    { source: 'Benoît', target: 'Beau' },
    { source: 'Benoît', target: 'Baudouin' },
    { source: 'Benoît', target: 'Bastien' },
    { source: 'Bastien', target: 'Baptiste' },
  ]
}
```

Quelques constantes

```javascript
const WIDTH = 500
const HEIGHT = 200
const RADIUS = 5
const COLORS = [
'#1f78b4',
'#33a02c',
'#e31a1c',
]
```

Les éléments

```javascript
const svg = d3.select('body').append('svg')
  .attr('viewBox', `0 0 ${WIDTH} ${HEIGHT}`)

// les liens entre les personnes
const edge = svg.selectAll('line')
  .data(DATA.edges)
  .enter()
  .append('line')
  .attr('stroke', 'black')

// les personnes
const node = svg.selectAll('circle')
  .data(DATA.nodes)
  .enter()
  .append('circle')
  .attr('r', RADIUS)
  .attr('fill', d => COLORS[d.group - 1])
```

Les éléments `edge` et `node` ne sont pas encore positionnées, elles le seront par la simulation

```javascript
const simulation = d3.forceSimulation()
  .force('link', d3.forceLink().id(d => d.id))
  .force('charge', d3.forceManyBody())
  .force('center', d3.forceCenter(WIDTH / 2, HEIGHT / 2))

// quand le graphique change, mettre à jour les "edge" et "node"
const onTick = () => {
  edge
    .attr('x1', d => d.source.x)
    .attr('y1', d => d.source.y)
    .attr('x2', d => d.target.x)
    .attr('y2', d => d.target.y)

  node
    .attr('cx', d => d.x)
    .attr('cy', d => d.y)
}

simulation
  .nodes(DATA.nodes)
  .on('tick', onTick)

simulation
  .force('link')
  .links(DATA.edges)
```

Événments quand un `node` est tiré

```javascript
const dragstarted = d => {
  if (!d3.event.active) {
    simulation.alphaTarget(0.3).restart()
  }
  d.fx = d.x
  d.fy = d.y
}

const dragged = d => {
  d.fx = d3.event.x
  d.fy = d3.event.y
}

const dragended = d => {
  if (!d3.event.active)  {
    simulation.alphaTarget(0)
  }
  d.fx = null
  d.fy = null
}
```

Ajouter l'événement `drag` à `node`

```javascript
const node = svg.selectAll('circle')
  .data(DATA.nodes)
  .enter()
  .append('circle')
  .attr('r', RADIUS)
  .attr('fill', d => COLORS[d.group - 1])
  // ici
  .call(d3.drag()
    .on('start', dragstarted)
    .on('drag', dragged)
    .on('end', dragended))
```

[Code](https://gitlab.com/anders-b/plan-dataviz-heig/tree/master/d3/assets/force.html)

[Résultat](http://dataviz-heig.surge.sh/files/d3/force.html)