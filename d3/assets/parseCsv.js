const fs = require('fs')

const csv = fs.readFileSync('votations.csv', 'utf-8')

const removeQuotes = string =>
  Array.from(string)
    .reduce((result, char) => char === '"' ? result : `${result}${char}`, '')

const parseLine = line => {
  const [canton, objet, ouiEnPc] = line.split(';')
  return {
    canton: removeQuotes(canton),
    objet: removeQuotes(objet),
    ouiEnPc: Number(ouiEnPc),
  }
}

const isCanton = ({ canton }) =>
  canton !== 'Suisse' && canton !== 'Canton'

const votations = csv.split('\n')
  .map(parseLine)
  .filter(isCanton)

console.log(votations)
